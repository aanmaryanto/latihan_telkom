import click
import os
import paramiko
import shutil
from datetime import datetime

@click.command()
@click.argument('path')
@click.option('--type', '-t', default="text", help='type of file')
@click.option('--output','-o', help='output location file')
def main(path, type, output):
    
    file_name = ""
    now = str(datetime.now().timestamp())
    if output is None:
        output = os.path.dirname(os.path.abspath(__file__))
        # print(output)
        if type == "text":
            file_name = now+".txt"
        elif type == "json":
            file_name = now+".json"
        else:
            click.echo("Does not match type")
            # print("Does not match type")
        file_output = output+'/'+ file_name
        # print(file_output)
        shutil.copy(path, file_output)
    else:
        shutil.copy(path, output)
        # print("not none")
if __name__ == "__main__":
    main()